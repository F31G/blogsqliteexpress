var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(Error(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

/**
 * create product
 */
app.post('/articles/addArticles', (req, res) => {
    const query = `INSERT INTO articles (title, thumbnail_url, text, author, date)
      VALUES ('${req.body.inputTitle}', 
          '${req.body.inputThumbNail}',
          '${req.body.inputText}',
          ${req.body.inputDate},
          ${req.body.inputAuthor}
      )`;

    db.run(query, (err) => {
        if (err) {
            return console.error('ici: ', err.message);
        }
        console.log('Products successfully created.');

        renderInventory(res);
    });
});


module.exports = app;